package org.eclipse.birt.report.utility.filename;


import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.eclipse.birt.report.utility.ParameterAccessor;
import org.eclipse.birt.report.utility.UrlUtility;


public class ExportFilenameGenerator extends DefaultFilenameGenerator
{
    public static final String PARAMETER_FILE_NAME = "SRW_REPORT_FILE";

    public ExportFilenameGenerator()
    {
        super();
    }

    private static boolean isBlank(String value)
    {
        return (value == null || value.trim().isEmpty());
    }

    private static String fileNameToASCII(String fileName)
    {
        if (!isBlank(fileName))
        {
            try
            {
                URI uri = new URI(null, null, fileName, null);
                return uri.toASCIIString();
            }
            catch (URISyntaxException ex)
            {
                // do nothing
            }
        }

        return fileName;
    }

    public String getFilename(String baseName, String extension,
        String outputType, Map options)
    {
        HttpServletRequest request = (HttpServletRequest) options
            .get(IFilenameGenerator.OPTIONS_HTTP_REQUEST);

        String fileName = baseName;

        if (request != null)
        {
            String pFileNameValue = ParameterAccessor.getParameter(request,
                PARAMETER_FILE_NAME);
            if (!isBlank(pFileNameValue))
            {
                fileName = fileNameToASCII(
                    UrlUtility.urlParamValueDecode(pFileNameValue));
            }
        }

        return makeFileName(fileName, extension);
    }
}
