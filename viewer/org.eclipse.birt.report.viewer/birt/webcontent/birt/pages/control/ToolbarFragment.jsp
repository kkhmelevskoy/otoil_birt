<%-----------------------------------------------------------------------------
	Copyright (c) 2004 Actuate Corporation and others.
	All rights reserved. This program and the accompanying materials 
	are made available under the terms of the Eclipse Public License v1.0
	which accompanies this distribution, and is available at
	http://www.eclipse.org/legal/epl-v10.html
	
	Contributors:
		Actuate Corporation - Initial implementation.
-----------------------------------------------------------------------------%>
<%@ page contentType="text/html; charset=utf-8" %>
<%@ page session="false" buffer="none" %>
<%@ page import="org.eclipse.birt.report.presentation.aggregation.IFragment,
				 org.eclipse.birt.report.resource.BirtResources,
				 org.eclipse.birt.report.utility.ParameterAccessor,
				 org.eclipse.birt.report.servlet.ViewerServlet" %>

<%-----------------------------------------------------------------------------
	Expected java beans
-----------------------------------------------------------------------------%>
<jsp:useBean id="fragment" type="org.eclipse.birt.report.presentation.aggregation.IFragment" scope="request" />
<jsp:useBean id="attributeBean" type="org.eclipse.birt.report.context.BaseAttributeBean" scope="request" />

<%-----------------------------------------------------------------------------
	Toolbar fragment
-----------------------------------------------------------------------------%>
	<TD NOWRAP>
		<DIV ID="toolbar"
	        <%
		        if( !attributeBean.isShowToolbar( ) )
		        {
		    %>
		        style="display:none"
		    <%
		        }
		    %>  
		>
			<TABLE CELLSPACING="1px" CELLPADDING="1px" CLASS="birtviewer_toolbar">
                <!--TR><TD></TD></TR-->
				<TR>
					<TD WIDTH="6px"/>
					<TD>
					   <BUTTON TYPE="button" NAME='toc'
					           TITLE="<%= BirtResources.getHtmlMessage( "birt.viewer.toolbar.toc" )%>"
					           CLASS="birtviewer_btn">
    					   <IMG SRC="birt/images/Toc.gif"
    					   		ALT="<%= BirtResources.getHtmlMessage( "birt.viewer.toolbar.toc" )%>" style="vertical-align: middle">
    					   <%= BirtResources.getHtmlMessage( "birt.viewer.toolbar.toc.btn" )%>
					   </BUTTON>
					</TD>
					<TD WIDTH="6px"/>
					<TD>
					   <BUTTON TYPE="button" NAME='parameter'
					           TITLE="<%= BirtResources.getHtmlMessage( "birt.viewer.toolbar.parameter" )%>"
					           CLASS="birtviewer_btn">
    					   <IMG SRC="birt/images/Report_parameters.gif"
    					   		ALT="<%= BirtResources.getHtmlMessage( "birt.viewer.toolbar.parameter" )%>" style="vertical-align: middle">
    					   <%= BirtResources.getHtmlMessage( "birt.viewer.toolbar.parameter.btn" )%>
					   </BUTTON>
					</TD>
					<TD WIDTH="6px"/>
					<TD>
					   <BUTTON TYPE="button" NAME='exportReport'
					           TITLE="<%= BirtResources.getHtmlMessage( "birt.viewer.toolbar.exportreport" )%>"
					           CLASS="birtviewer_btn">
    					   <IMG SRC="birt/images/ExportReport.gif"
    					   		ALT="<%= BirtResources.getHtmlMessage( "birt.viewer.toolbar.exportreport" )%>" style="vertical-align: middle">
    					   <%= BirtResources.getHtmlMessage( "birt.viewer.toolbar.exportreport.btn" )%>
					   </BUTTON>
					</TD>
					<TD WIDTH="6px"/>
					<TD>
					   <BUTTON TYPE="button" NAME='print'
					           TITLE="<%= BirtResources.getHtmlMessage( "birt.viewer.toolbar.print" )%>"
					           CLASS="birtviewer_btn">
    					   <IMG SRC="birt/images/Print.gif"
    					   		ALT="<%= BirtResources.getHtmlMessage( "birt.viewer.toolbar.print" )%>" style="vertical-align: middle">
    					   <%= BirtResources.getHtmlMessage( "birt.viewer.toolbar.print.btn" )%>
					   </BUTTON>
					</TD>
					<!--TD ALIGN='right'>
					</TD-->
					<TD WIDTH="6px"/>
				</TR>
			</TABLE>
		</DIV>
	</TD>
